import { Col, Container, Row } from "react-bootstrap";
import "./App.css";
import NavMenu from "./components/NavMenu";
import { Card, Button } from "react-bootstrap";
import React, { Component } from "react";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      data: [
        {
          img:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Ankor_Wat_temple.jpg/1200px-Ankor_Wat_temple.jpg",
          title: "angkor",
          dec: "in cambodia",
          isDeleted: false,
        },
        {
          img:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Ankor_Wat_temple.jpg/1200px-Ankor_Wat_temple.jpg",
          title: "taprom",
          dec: "in cambodia",
          isDeleted: false,
        },
        {
          img:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Ankor_Wat_temple.jpg/1200px-Ankor_Wat_temple.jpg",
          title: "bonteay srey",
          dec: "in cambodia",
          isDeleted: false,
        },
      ],
    };
  }
  delete(index) {
    let newData = [...this.state.data];
    newData[index].isDeleted = true;
    newData = newData.filter((item) => item.isDeleted === false);
    this.setState(
      {
        data: newData,
      },
      () => {
        console.log(this.state.data);
        console.log(newData);
      }
    );
  }
  render() {
    return (
      <>
        <NavMenu />
        <Container>
          <Row>
            {this.state.data.map((obj, index) => (
              <Col key={index} md={3} sm={6}>
                <Card>
                  <Card.Img variant="top" src={obj.img} />
                  <Card.Body>
                    <Card.Title>{obj.title}</Card.Title>
                    <Card.Text>{obj.dec}</Card.Text>
                    <Button variant="danger" onClick={() => this.delete(index)}>
                      delete
                    </Button>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
        </Container>
      </>
    );
  }
}
